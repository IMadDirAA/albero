//
// Created by imadd on 14/02/2020.
//

#include "tree.h"

struct node* createNode(int data, int level, struct node *parent) {
    struct node *newNode = malloc(sizeof(struct node));
    if (!newNode)
        return NULL;
    newNode->data = data;
    newNode->level = level;
    newNode->parent = parent;
    newNode->bilancio = 0;
    newNode->left = newNode->right = NULL;
    return newNode;
}

bool insert(struct node **root, int data) {
    static struct node *parent = NULL;
    if (root == NULL)
        return false;
    if (*root == NULL) {
        *root = createNode(data, (parent ? parent->level + 1 : 0), parent);
        parent = NULL;
        return *root;
    }
    parent = *root;
    if (data < (*root)->data)
        return insert(&((*root)->left), data);
    if (data > (*root)->data)
        return insert(&((*root)->right), data);
    parent = NULL;
    return false;
}

bool search(struct node *root, int date) {
    if (root == NULL)
        return false;
    if (date == root->data)
        return true;
    if (date < root->data)
        return search(root->left, date);
    if (date > root->data)
        return search(root->right, date);
}

void inOrder(struct node *root) {
    if(root != NULL) {
        inOrder(root->left);
        printf("(D-%d, L-%d, P-%d) ->", root->data, root->level, (root->parent ? root->parent->data : -1));
        inOrder(root->right);
    }
}

void preOrder(struct node *root) {
    if(root != NULL) {
        printf("(D-%d, L-%d, P-%d) ->", root->data, root->level, (root->parent ? root->parent->data : -1));
        preOrder(root->left);
        preOrder(root->right);
    }
}

void postOrder(struct node *root) {
    if(root != NULL) {
        postOrder(root->left);
        postOrder(root->right);
        printf("(D-%d, L-%d, P-%d) ->", root->data, root->level, (root->parent ? root->parent->data : -1));
    }
}

void deleteTree(struct node *root) {
    if (root != NULL) {
        deleteTree(root->right);
        deleteTree(root->left);
        free(root);
    }
}

struct node *deleteNode(struct node **nodeToBeDeleted) {
    // 1). check if there is a data to be processed
    if (nodeToBeDeleted == NULL || *nodeToBeDeleted == NULL)
        return NULL;
    // 2). find the max node in the left tree
    struct node **maxLeftNode = getMaxNode(&((*nodeToBeDeleted)->left));
    // 2). or find the min node in the right tree
    // example: struct node **minRightNode = getMinNode(&((*nodeToBeDeleted)->right));

    /** @var struct node *new root which represent the new pointer to replace the delete node*/
    struct node *newRoot = NULL;

    // 3). check if there is a max left node
    if (maxLeftNode) {
        // update tree and the newRoot pointer
        newRoot = *maxLeftNode;
        if ((*maxLeftNode)->left)
            (*maxLeftNode)->left->parent = (*maxLeftNode)->parent;
        *maxLeftNode = (*maxLeftNode)->left;
        decrementTreeLevel(*maxLeftNode);
        newRoot->right = (*nodeToBeDeleted)->right;
        newRoot->left = (*nodeToBeDeleted)->left;
        if (newRoot->right)
            newRoot->right->parent = newRoot;
        if (newRoot->left)
            newRoot->left->parent = newRoot;
    }
    // 3). else update the newRoot with the right tree of the node to be deleted
    else
        newRoot = (*nodeToBeDeleted)->right;
    // update new root parent and level
    newRoot->parent = (*nodeToBeDeleted)->parent;
    newRoot->level = (*nodeToBeDeleted)->level;
    // 4). delete node
    free(*nodeToBeDeleted);
    *nodeToBeDeleted = NULL;
    return newRoot;
}

struct node **getMaxNode(struct node **root) {
    if (root == NULL || *root == NULL)
        return NULL;
    while ((*root)->right)
        *root = (*root)->right;
    return root;
}

struct node **getMinNode(struct node **root) {
    if (root == NULL || *root == NULL)
        return NULL;
    while ((*root)->left)
        *root = (*root)->left;
    return root;
}

bool pop(struct node **root, int date) {
    if (root == NULL || *root == NULL)
        return false;
    struct node **nodeToBeDeleted = searchNode(root, date);
    if (nodeToBeDeleted) {
        *nodeToBeDeleted = deleteNode(nodeToBeDeleted);
        return true;
    }
    return false;
}

struct node **searchNode(struct node **root, int date) {
    if (root == NULL || *root == NULL)
        return NULL;
    if (date == (*root)->data)
        return root;
    if (date < (*root)->data)
        return searchNode(&((*root)->left), date);
    if (date > (*root)->data)
        return searchNode(&((*root)->right), date);
}

int nodesLength(struct node *root) {
    if (!root)
        return 0;
    return (root->right || root->left) + nodesLength(root->right) + nodesLength(root->left);
}

int sheetsLength(struct node *root) {
    if (root == NULL)
        return 0;
    if (!(root->left) && !(root->right))
        return 1;
    return sheetsLength(root->right) + sheetsLength(root->left);
}

int treeLength(struct node *root) {
    if (!root)
        return 0;
    return 1 + treeLength(root->right) + treeLength(root->left);
}

int nodesSum(struct node *root) {
    if (!root)
        return 0;
    return ((root->left || root->right) ? root->data : 0) + nodesSum(root->right) + nodesSum(root->left);
}

int sheetsSum(struct node *root) {
    if (root == NULL)
        return 0;
    if (!(root->left) && !(root->right))
        return root->data;
    return sheetsSum(root->right) + sheetsSum(root->left);
}

int treeSum(struct node *root) {
    if (!root)
        return 0;
    return root->data + treeSum(root->right) + treeSum(root->left);
}

float averageTree(struct node *root) {
    if (!root)
        return 0;
    return (float)treeSum(root) / (float)treeLength(root);
}

float averageNodes(struct node *root, bool *ok) {
    int nodes_length = nodesLength(root);
    if (!root || !nodes_length)
        return *ok = false;
    return (float)nodesSum(root) / (float)nodes_length;
}

float averageSheets(struct node *root, bool *ok) {
    int sheets_length = sheetsLength(root);
    if (!root || !sheets_length)
        return *ok = false;
    return (float)sheetsSum(root) / (float)sheets_length;
}

void decrementTreeLevel(struct node *root) {
    if (!root)
        return;
    if (root->parent)
        root->level--;
    decrementTreeLevel(root->left);
    decrementTreeLevel(root->right);
}
