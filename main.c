#include <stdio.h>
#include "tree.h"
int main() {
    struct node *root = NULL;
    int searchDate = 90;
    int popedDate = 50;
    int appoggio = 0;
    int data[17] = {50, 40, 60, 30, 44, 55, 70, 20, 35, 42, 46, 80, 41, 43, 45, 48, 90};
    for (int i = 0; i < 17; ++i)
        insert(&root, data[i]);
    preOrder(root);
    printf("\ntree length = %d\n", treeLength(root));
    printf("nodes length = %d\n", nodesLength(root));
    printf("sheets length = %d\n", nodesLength(root));
    printf("tree sum = %d\n", treeSum(root));
    printf("nodes sum = %d\n", nodesSum(root));
    printf("sheets sum = %d\n", sheetsSum(root));
    if (pop(&root, popedDate)) {
        printf("\nelement %d is poped\n", popedDate);
        preOrder(root);
    } else {
        printf("\nelement %d is not poped\n", popedDate);
    }

    if (search(root, searchDate)) {
        printf("\nnumber %d is found\n", searchDate);
    } else {
        printf("\nnumber %d is not found\n", searchDate);
    }
    return 0;
}