#ifndef SET_H
#define SET_H

#include<stdio.h>
#include<stdlib.h>
#include <stdbool.h>

/**
 * struct node
 * @details the node structure
 * @var int data which represent the node data
 * @var struct node* left which represent the left tree
 * @var struct node* right which represent the right tree
 */
struct node {
    int data;
    int level;
    int bilancio;
    struct node* parent;
    struct node* left;
    struct node* right;
};

/**
 * createNode()
 * @param int data
 * @param int level
 * @param struct node *parent
 * @details the function create a node with the data value
 * @return struct node *
 */
struct node *createNode(int data, int level, struct node *parent);

/**
 * insert()
 * @details the function insert a integer data in order in the tree
 * @param struct node **root
 * @param int data
 * @return bool
 */
bool insert(struct node **root, int data);

/**
 * search()
 * @param struct node *root which is the tree head(pointer)
 * @param int data which is the data to search in the tree
 * @param bool* ok which indicate if the result was found(true) or not(false)
 */
bool search(struct node *root, int data);

/**
 * searchNode()
 * @param struct node **root which is the tree head(pointer)
 * @param int data which is the data to search in the tree
 * @return
 */
struct node **searchNode(struct node **root, int data);

/**
 * inOrder()
 *
 */
void inOrder(struct node *root);
void preOrder(struct node *root);
void postOrder(struct node *root);
void deleteTree(struct node *root);
struct node *deleteNode(struct node **nodeToBeDeleted);
/** @todo */
struct node *mergeTree(struct node *root1, struct node *root2);
struct node **getMaxNode(struct node **root);
struct node **getMinNode(struct node **root);
bool pop(struct node **root, int data);
int nodesLength(struct node *root);
int sheetsLength(struct node *root);
int treeLength(struct node *root);
int nodesSum(struct node *root);
int sheetsSum(struct node *root);
int treeSum(struct node *root);
float averageTree(struct node *root);
float averageNodes(struct node *root, bool *ok);
float averageSheets(struct node *root, bool *ok);
void decrementTreeLevel(struct node *root);
#endif